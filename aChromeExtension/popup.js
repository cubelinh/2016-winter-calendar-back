$(document).ready(function() {
    chrome.extension.onMessage.addListener(function(message, messageSender, sendResponse) {
        if (message.data = "UpdateUI") {
            console.log(message);
            displayGoals();
        }
    });
    displayGoals();
    var loginApiUrl = 'https://wintercalendar.herokuapp.com/api/v1/auth/sign_in';
    $("#login").click(function() {
        var email1 = $("#email").val();
        var password1 = $("#password").val();
        // Checking for blank fields.
        if (email1 == '' || password1 == '') {
            $('input[type="text"],input[type="password"]').css("border", "2px solid red");
            $('input[type="text"],input[type="password"]').css("box-shadow", "0 0 3px red");
            alert("Please fill all fields...!!!!!!");
        } else {
            $.post(loginApiUrl, {
                    email: email1,
                    password: password1
                })
                .done(function(data) {
                    console.log(data);
                    chrome.runtime.sendMessage(data);
                })
                .fail(function(xhr, status, error) {
                    alert(error);
                });
        }
    }); // End of $login

    $("#refresh").click(function() {
        refresh();
    }); // End of $refresh

    $("#logout").click(function() {
        logout();
    }); // End of $logout
});

function logout() {
    var token;
    chrome.storage.local.get('token', function(data) {
        token = data.token;
        if (token && token != null) {
            var urlCall = "https://wintercalendar.herokuapp.com/api/v1/auth/sign_out";
            $.ajax({
                url: urlCall + '?' + $.param({
                    "token": token
                }),
                type: 'DELETE',
                success: function() {
                    chrome.storage.local.clear(function() {
                        console.log("clear done");
                        var error = chrome.runtime.lastError;
                        if (error) {
                            console.error(error);
                        }
                    });
                    alert("logout success!");
                    $("table").hide();
                    $("#refresh").hide();
                    $("#logout").hide();
                    $(".form").show();
                } || $.noop,
                error: function() {
                    console.log("logout failed!");
                } || $.noop
            });
        } // End of if
    }); // End of chrome.storage
}

function refresh() {
    chrome.storage.local.get('token', function(data) {
        token = data.token;
        if (token && token != null) {
            remind(token);
        } // End of if
    }); // End of chrome.storage
}

function displayGoals() {
    chrome.storage.local.get('token', function(data) {
        token = data.token;
        if (token && token != null) {
            // Get goals from storage
            chrome.storage.local.get('goals', function(data) {
                var goals = data.goals;
                if (goals && goals != null) {
                    console.log(data);
                    console.log(goals);
                    addBadgeText(goals.length);
                    var x = document.getElementsByClassName("main");

                    var table = document.createElement('table');
                    table.setAttribute('border', '1');
                    table.setAttribute('width', '100%');

                    var row = table.insertRow(0);
                    createCell(row, "Goal", 0);
                    createCell(row, "Name", 1);
                    createCell(row, "Description", 2);
                    createCell(row, "Repeatition", 3);
                    createCell(row, "Progress", 4);
                    createCell(row, "Current chain", 5);
                    createCell(row, "Best chain", 6);
                    createCell(row, "Start", 7);

                    for (var i = 1; i < goals.length + 1; i++) {
                        var row = table.insertRow(i);
                        createCell(row, i + "", 0);
                        createCell(row, goals[i - 1].goal_name, 1);
                        createCell(row, goals[i - 1].description, 2);

                        var typeOfRepeatition;
                        switch (goals[i - 1].repetition.type_of_repetition) {
                            case 1:
                                typeOfRepeatition = "Daily";
                                break;
                            case 2:
                                typeOfRepeatition = "Weekly";
                                break;
                            case 3:
                                typeOfRepeatition = "Monthly";
                                break;
                            case 4:
                                typeOfRepeatition = "Annual";
                                break;
                            default:
                                typeOfRepeatition = "";
                        }
                        createCell(row, typeOfRepeatition, 3);
                        createCell(row, goals[i - 1].progress, 4);
                        createCell(row, goals[i - 1].current_chain, 5);
                        createCell(row, goals[i - 1].best_chain, 6);

                        var hourMinute = goals[i - 1].start_date.split("T");
                        hourMinute = hourMinute[1].split(":");
                        hourMinute = hourMinute[0] + ":" + hourMinute[1];
                        createCell(row, hourMinute, 7);
                    }

                    $("table").remove();
                    $(".form").hide();
                    console.log("table is: ");
                    console.log(table);

                    console.log("x[0] is: ");
                    console.log(x[0]);
                    x[0].appendChild(table);
                    $("#refresh").show();
                    $("#logout").show();
                }

            }); // End of chrome.storage
        } else {
            $("#refresh").hide();
            $("#logout").hide();
        } // End of if
    }); // End of chrome.storage
}
