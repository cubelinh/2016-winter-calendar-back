function addBadgeText(numberOfGoals) {
    chrome.browserAction.setBadgeText({
        text: numberOfGoals.toString()
    });
}

var token;
var todayGoalApiUrl = 'https://wintercalendar.herokuapp.com/api/v1/goals/today';
chrome.extension.onMessage.addListener(function(message, messageSender, sendResponse) {
    if (message.data.token && message.data.token != null) {
        remind(message.data.token);
        // Save token to Chrome storage
        chrome.storage.local.set({
            'token': message.data.token
        }, function() {
            // Notify that we saved.
            console.log('Token saved');
        });
    }
});

function remind(token) {
    var reminders = [];
    $.get(todayGoalApiUrl + '?token=' + token, function(data) {
        //console.log(data);
        for (var i = 0; i < data.length; i++) {
            var time = data[i].start_date.split("T");
            time = time[1].split(".");
            time = time[0].split(":");

            for (var k = 0; k < time.length; k++) {
                time[k] = parseInt(time[k]);
            }

            if (data[i].time_before != null) {
                var timeList = data[i].time_before.split(",");
                for (var j = 0; j < timeList.length; j++) {
                    timeList[j] = timeList[j].trim();
                    if (timeList[j].length == 5) {
                        console.log("timeList[j]" + timeList[j]);
                        timeList[j] = timeList[j].split(":");
                        var newTime = new Date();
                        var startTime = new Date(newTime.getFullYear(), newTime.getMonth(), newTime.getDate(), time[0], time[1], time[2]).getTime();
                        var beforeTime = new Date(newTime.getFullYear(), newTime.getMonth(), newTime.getDate(), parseInt(timeList[j][0]), parseInt(timeList[j][1]), 0).getTime();
                        var timeBefore = startTime - beforeTime;
                        if (timeBefore >= 0) {
                            timeList[j] = timeBefore / (1000 * 60);
                        }
                    }
                    if (!isNaN(timeList[j])) {
                        reminders.push({
                            "id": data[i].id,
                            "goalName": data[i].goal_name,
                            "description": data[i].description,
                            "time": time,
                            "timeBefore": parseInt(timeList[j])
                        });
                    };
                }
            }
        }
        console.log(reminders);
        chrome.storage.local.set({
            'reminders': reminders
        }, function() {
            // Notify that we saved.
            console.log('reminders saved');
        });

        chrome.storage.local.set({
            'goals': data
        }, function() {
            // Notify that we saved.
            console.log('goals saved');
        });

        //Update UI
        chrome.runtime.sendMessage({
            data: "UpdateUI"
        });
    }).fail(function(xhr, status, error) {
        alert(error);
        $("table").hide();
        $("#refresh").hide();
        $(".form").show();
    });
}

var timeouts = []; // Container of timeout may be used for clearTimeout
chrome.storage.onChanged.addListener(function(changes, namespace) {
    for (key in changes) {
        if (key == "goals") {
            console.log("changes on key goals");
        }

        if (key == "reminders") {
            console.log("changes on key reminders");
            // Clear timeouts if having any changes
            console.log("timeout before");
            console.log(timeouts);

            for (var i = 0; i < timeouts.length; i++) {
                clearTimeout(timeouts[i]);
                console.log("clearTimeout " + timeouts[i]);
            }

            timeouts = [];

            // Get reminders from storage
            chrome.storage.local.get('reminders', function(data) {
                var reminders = data.reminders;
                if (reminders && reminders != null) {
                    console.log("reminders (onChanged): ");
                    console.log(reminders);
                    for (var i = 0; i < reminders.length; i++) {
                        var cnm;
                        switch (reminders[i].timeBefore) {
                            case 0:
                                cnm = "Do it right now!"
                                break;
                            case 1:
                                cnm = "1 minute to go!"
                                break;
                            default:
                                cnm = reminders[i].timeBefore + " minutes to go!"

                        }
                        var options = {
                            type: "basic",
                            title: reminders[i].goalName,
                            message: reminders[i].description,
                            contextMessage: cnm,
                            iconUrl: "icon.png"
                        };

                        var currentTime = new Date();
                        var id = currentTime.toString() + reminders[i].id;
                        //id = id.split("(ICT)");
                        //id = id[1];
                        //console.log(id);

                        var timeout = creatTimeout(reminders[i].time, reminders[i].timeBefore, id, options);
                        if (timeout != null) {
                            console.log("Add time out to timeouts array: " + timeout);
                            timeouts.push(timeout);
                            console.log("timeouts.length " + timeouts.length);
                        }
                    }
                }
            });
        }
    }
});

function creatTimeout(time, timeBefore, id, options) {
    if (time.length != 3) {
        return
    }
    var newTime = new Date();
    var difTimeBase = new Date(newTime.getFullYear(), newTime.getMonth(), newTime.getDate(), time[0], time[1], time[2]).getTime() - Date.now();
    if (difTimeBase >= 0) {
        var difTime = difTimeBase - timeBefore * 60 * 1000;
        var timeout;
        if (difTime >= 0) {
            console.log("createTimeout difTime > 0");
            timeout = setTimeout(function() {
                chrome.notifications.create(id, options);
            }, difTime);
            return timeout;
        }
    }
}


function createCell(row, textNode, cellIndex) {
    var text = document.createTextNode(textNode);
    var cell = row.insertCell(cellIndex);
    cell.setAttribute('align', 'center');
    cell.appendChild(text);
}

//chrome.notifications.onButtonClicked.addListener(redirectWindow);
chrome.notifications.onClicked.addListener(redirectWindow);

function redirectWindow(notificationId) {
    var currentDate = new Date();

    var id = notificationId.split("(ICT)");
    id = id[1];
    window.open('http://seinfeld-calendar-v2.herokuapp.com/#/monthview/' + id + "/" + currentDate.getFullYear() + "/" + (currentDate.getMonth() + 1));
};
