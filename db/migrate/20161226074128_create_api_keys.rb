class CreateApiKeys < ActiveRecord::Migration[5.0]
    def change
        create_table :api_keys do |t|
            t.string :access_token, null: false
            t.integer :myuser_id, null: false
            t.boolean :active, null: false, default: true
            t.datetime :expires_at

            t.timestamps
        end

        add_index :api_keys, ['myuser_id'], name: 'index_api_keys_on_myuser_id', unique: false
        add_index :api_keys, ['access_token'], name: 'index_api_keys_on_access_token', unique: true
    end
end
