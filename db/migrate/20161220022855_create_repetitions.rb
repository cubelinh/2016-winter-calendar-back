class CreateRepetitions < ActiveRecord::Migration[5.0]
  def change
    create_table :repetitions do |t|
      t.references :goal, foreign_key: true
      t.integer :type_of_repetition
      t.integer :how_often
      t.string :day_of_week
      t.integer :type_of_month

      t.timestamps
    end
  end
end
