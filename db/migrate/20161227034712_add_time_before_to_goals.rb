class AddTimeBeforeToGoals < ActiveRecord::Migration[5.0]
    def change
        add_column :goals, :time_before, :string
    end
end
