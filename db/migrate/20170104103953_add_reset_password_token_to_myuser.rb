class AddResetPasswordTokenToMyuser < ActiveRecord::Migration[5.0]
  def change
    add_column :myusers, :reset_password_token, :string
  end
end
