class AddIndexToMyusersEmail < ActiveRecord::Migration[5.0]
    def change
        add_index :myusers, :email, unique: true
    end
end
