class AddTimeBeforeToReminders < ActiveRecord::Migration[5.0]
  def change
    add_column :reminders, :time_before, :datetime
  end
end
