class ChangeMinuteBeforeTypeToInteger < ActiveRecord::Migration[5.0]
    def change
        change_column :reminders, :minute_before, 'integer USING CAST(minute_before AS integer)'
    end
end
