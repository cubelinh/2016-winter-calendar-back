class CreateReminders < ActiveRecord::Migration[5.0]
  def change
    create_table :reminders do |t|
      t.references :goal, foreign_key: true
      t.string :time_before

      t.timestamps
    end
  end
end
