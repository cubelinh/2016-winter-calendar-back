class CreateEndDates < ActiveRecord::Migration[5.0]
  def change
    create_table :end_dates do |t|
      t.references :goal, foreign_key: true
      t.integer :type_of_end_date
      t.datetime :specific_end_date
      t.integer :number_of_event

      t.timestamps
    end
  end
end
