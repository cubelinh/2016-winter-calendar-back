class CreateGoals < ActiveRecord::Migration[5.0]
  def change
    create_table :goals do |t|
      t.references :user, foreign_key: true
      t.string :goal_name
      t.datetime :start_date
      t.string :description
      t.boolean :is_favorite, :default => false
      t.boolean :is_archived, :default => false
      t.boolean :is_auto, :default => true
      t.integer :best_chain, :default => 0
      t.string :progress

      t.timestamps
    end
  end
end
