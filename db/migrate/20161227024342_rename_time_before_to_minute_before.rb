class RenameTimeBeforeToMinuteBefore < ActiveRecord::Migration[5.0]
    def change
        rename_column :reminders, :time_before, :minute_before
    end
end
