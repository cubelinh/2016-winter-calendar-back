class CreateCalendars < ActiveRecord::Migration[5.0]
  def change
    create_table :calendars do |t|
      t.references :goal, foreign_key: true
      t.datetime :date_of_calendar
      t.integer :status

      t.timestamps
    end
  end
end
