class ChangeGoalDefault < ActiveRecord::Migration[5.0]
  def change
    change_column_default :goals, :is_auto, false
    change_column_default :goals, :is_archived, false
    change_column_default :goals, :is_favorite, false
  end
end
