class AddEmailConfirmationToMyuser < ActiveRecord::Migration[5.0]
  def change
    add_column :myusers, :is_confirmed, :boolean, :default => false
    add_column :myusers, :confirmed_token, :string
  end
end
