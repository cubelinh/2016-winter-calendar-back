class AddCurrentChainToGoals < ActiveRecord::Migration[5.0]
  def change
    add_column :goals, :current_chain, :integer, :default => 0
  end
end
