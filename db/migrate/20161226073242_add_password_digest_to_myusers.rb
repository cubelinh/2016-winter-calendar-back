class AddPasswordDigestToMyusers < ActiveRecord::Migration[5.0]
  def change
    add_column :myusers, :password_digest, :string
  end
end
