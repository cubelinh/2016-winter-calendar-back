# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170105050100) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "api_keys", force: :cascade do |t|
    t.string   "access_token",                null: false
    t.integer  "myuser_id",                   null: false
    t.boolean  "active",       default: true, null: false
    t.datetime "expires_at"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.index ["access_token"], name: "index_api_keys_on_access_token", unique: true, using: :btree
    t.index ["myuser_id"], name: "index_api_keys_on_myuser_id", using: :btree
  end

  create_table "calendars", force: :cascade do |t|
    t.integer  "goal_id"
    t.datetime "date_of_calendar"
    t.integer  "status"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.index ["goal_id"], name: "index_calendars_on_goal_id", using: :btree
  end

  create_table "end_dates", force: :cascade do |t|
    t.integer  "goal_id"
    t.integer  "type_of_end_date"
    t.datetime "specific_end_date"
    t.integer  "number_of_event"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.index ["goal_id"], name: "index_end_dates_on_goal_id", using: :btree
  end

  create_table "goals", force: :cascade do |t|
    t.string   "goal_name"
    t.datetime "start_date"
    t.string   "description"
    t.boolean  "is_favorite",   default: false
    t.boolean  "is_archived",   default: false
    t.boolean  "is_auto",       default: false
    t.integer  "best_chain",    default: 0
    t.string   "progress"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.string   "time_before"
    t.integer  "myuser_id"
    t.integer  "current_chain", default: 0
    t.index ["myuser_id"], name: "index_goals_on_myuser_id", using: :btree
  end

  create_table "myusers", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.string   "password_digest"
    t.string   "reset_password_token"
    t.boolean  "is_confirmed",         default: false
    t.string   "confirmed_token"
    t.index ["email"], name: "index_myusers_on_email", unique: true, using: :btree
  end

  create_table "reminders", force: :cascade do |t|
    t.integer  "goal_id"
    t.integer  "minute_before"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.datetime "time_before"
    t.index ["goal_id"], name: "index_reminders_on_goal_id", using: :btree
  end

  create_table "repetitions", force: :cascade do |t|
    t.integer  "goal_id"
    t.integer  "type_of_repetition"
    t.integer  "how_often"
    t.string   "day_of_week"
    t.integer  "type_of_month"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.index ["goal_id"], name: "index_repetitions_on_goal_id", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "provider",               default: "email", null: false
    t.string   "uid",                    default: "",      null: false
    t.string   "encrypted_password",     default: "",      null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,       null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.string   "name"
    t.string   "nickname"
    t.string   "image"
    t.string   "email"
    t.json     "tokens"
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.index ["email"], name: "index_users_on_email", using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
    t.index ["uid", "provider"], name: "index_users_on_uid_and_provider", unique: true, using: :btree
  end

  add_foreign_key "calendars", "goals"
  add_foreign_key "end_dates", "goals"
  add_foreign_key "goals", "myusers"
  add_foreign_key "reminders", "goals"
  add_foreign_key "repetitions", "goals"
end
