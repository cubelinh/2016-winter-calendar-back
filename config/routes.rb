Rails.application.routes.draw do
    root 'welcome#index'
    mount API::Base, at: '/'
    mount GrapeSwaggerRails::Engine, at: '/documentation'
end
