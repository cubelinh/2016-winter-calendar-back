module API
    module V1
        class Apikeys < Grape::API
            include API::V1::Defaults

            resource :auth do
                desc 'Creates new user then return info and token'
                params do
                    requires :name, type: String, desc: 'Name'
                    requires :email, type: String, desc: 'Email address'
                    requires :password, type: String, desc: 'Password'
                    # requires :password_confirmation, type: String, desc: 'Password confirmation'
                end
                post :sign_up do
                    myuser = Myuser.create!(name: params[:name], email: params[:email], password: params[:password])
                    myuser.update_confirmed_token!
                    if myuser
                        UserMailer.sent_active_link(myuser).deliver
                        # key = ApiKey.create!(myuser_id: myuser.id)
                        # { status: 201,
                        #   data: { token: key.access_token, name: myuser.name, email: myuser.email } }
                        { status: 200, data: { confirmed_token: myuser.confirmed_token } }
                    else
                        myuser.error
                    end
                end

                desc 'Confirm email'
                params do
                    requires :confirmed_token, type: String, desc: 'Confirmed token'
                end
                post :confirm_email do
                    myuser = Myuser.find_by_confirmed_token(params[:confirmed_token])
                    if myuser && myuser.update_column(:is_confirmed, true)
                        key = ApiKey.create!(myuser_id: myuser.id)
                        { status: 200,
                          data: { token: key.access_token, name: myuser.name, email: myuser.email } }
                    else
                        myuser.error
                    end
                end

                desc 'Creates and returns access_token if valid login'
                params do
                    requires :email, type: String, desc: 'email address'
                    requires :password, type: String, desc: 'Password'
                end
                post :sign_in do
                    myuser = Myuser.find_by_email(params[:email].downcase)
                    # return myuser
                    if myuser && !myuser.is_confirmed
                        error!('Not actived yet', 401)
                    elsif myuser && myuser.authenticate(params[:password])
                        # return myuser
                        key = ApiKey.create!(myuser_id: myuser.id)
                        # return key
                        { status: 200,
                          data: { token: key.access_token, name: myuser.name, email: myuser.email } }
                    else
                        error!('Unauthorized.', 401)
                    end
                end

                desc 'validate_token'
                params do
                    requires :token, type: String, desc: 'Access token.'
                end
                get :validate_token do
                    authenticate!
                    { status: 200,
                      data: { name: @current_user.name, email: @current_user.email } }
                end

                desc 'logout'
                params do
                    requires :token, type: String, desc: 'Access token.'
                end
                delete :sign_out do
                    authenticate!
                    @current_token.destroy
                    {
                        status: 200
                    }
                end

                desc 'send email to reset password'
                params do
                    requires :email, type: String, desc: 'User email'
                end
                post :reset_password do
                    user = Myuser.find_by_email(params[:email])
                    if user
                        user.update_reset_password_token!
                        UserMailer.sent_forgot_password(user).deliver
                        user.reset_password_token
                    else
                        error!('Wrong email', 404)
                    end
                end

                desc 'set new password'
                params do
                    requires :reset_password_token, type: String, desc: 'Reset password token'
                    requires :new_password, type: String, desc: 'New password'
                end
                post :new_password do
                    user = Myuser.find_by_reset_password_token(params[:reset_password_token])
                    if user && user.update!(password: params[:new_password], reset_password_token: nil)
                        { status: 200,
                          data: { name: user.name, email: user.email } }
                    else
                        error!('Wrong token', 404)
                    end
                end

                desc 'delete account for testing purpose'
                params do
                    requires :email, type: String, desc: 'email'
                end
                delete :email do
                    Myuser.find_by_email(params[:email]).destroy
                end

                desc 'test'
                params do
                    # requires :date, type: DateTime, desc: 'Date.'
                    requires :token, type: String, desc: 'Access token.'
                end
                get :test do
                    # (DateTime.now - params[:date]).to_i % 2
                    # DateTime.now.cweek
                    # DateTime.now.wday
                    # 'Mon, Tue, Wed'.include?('Wed')
                    # Date::DAYNAMES[params[:date].wday][0..2]
                    # date = DateTime.now.strftime('%Y').to_i
                    # date = DateTime.now.strftime('%m').to_i
                    # date = DateTime.now.strftime('%d').to_i
                    # date = DateTime.now.strftime('%m-%d')
                    # 'Ali'.eql? 'Ali'
                    # @goal = Goal.find(10) # 2016-12-01
                    # @goal.is_the_date_of_goal?(params[:date])
                    # @goal.test(DateTime.now.to_date)
                    # DateTime.now.to_date
                    msg = { msg: 'Hello Nhi' }
                    $redis.publish 'rt-change', msg.to_json
                end
            end
          end
    end
end
