module API
    module V1
        class Base < Grape::API
            mount API::V1::Goals
            mount API::V1::Apikeys
            # mount API::V1::AnotherResource
        end
    end
end
