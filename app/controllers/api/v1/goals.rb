module API
    module V1
        class Goals < Grape::API
            include API::V1::Defaults
            resource :goals do
                # Linh goes here
                desc 'Return list of goals today'
                params do
                    requires :token, type: String, desc: 'Access token.'
                end
                get '/today' do
                    authenticate!
                    @goal_list = @current_user.goals.where(is_archived: false)
                    result_list = []
                    @goal_list.each do |goal|
                        result_list.push(goal) if goal.is_the_date_of_goal?(DateTime.now.to_date)
                        # result_list.push(goal.is_the_date_of_goal?(DateTime.now.to_date).to_s + goal.id.to_s)
                    end
                    result_list
                    # {data: result_list}
                end
                # Linh ends here

                desc 'Return all goals'
                params do
                    requires :token, type: String, desc: 'Access token.'
                    requires :sort_type, type: Integer, desc: '1: created, 2: name, 3: started, 4: frequency, 5: current chain, 6: best chain, 7: ratio'
                    requires :is_reversed, type: Boolean, desc: 'Reverse order'
                end
                get '', root: :goals do
                    authenticate!
                    @current_user.goals.all.each(&:create_track)
                    type = params[:is_reversed] ? :desc : :asc
                    @goals = @current_user.goals.all.order(is_favorite: :desc)
                    case params[:sort_type]
                    when 1 then @goals.order(id: type)
                    when 2 then @goals.order(goal_name: type)
                    when 3 then @goals.order(start_date: type)
                    when 4 then @goals.includes(:repetition).order('repetitions.type_of_repetition ' + type.to_s)
                    when 5 then @goals.order(current_chain: type)
                    when 6 then @goals.order(best_chain: type)
                    when 7 then
                        @goals_favorite = @current_user.goals.where(is_favorite: true)
                        @goals_not_favorite = @current_user.goals.where(is_favorite: false)
                        if params[:is_reversed]
                            @goals_favorite = @goals_favorite.sort_by { |g| [-g.progress.to_r.to_f, g.id] }
                            @goals_not_favorite = @goals_not_favorite.sort_by { |g| [-g.progress.to_r.to_f, g.id] }
                        else
                            @goals_favorite = @goals_favorite.sort_by { |g| [g.progress.to_r.to_f, g.id] }
                            @goals_not_favorite = @goals_not_favorite.sort_by { |g| [g.progress.to_r.to_f, g.id] }
                        end
                        @goals_favorite + @goals_not_favorite
                    end
                end

                desc 'Return a goals'
                params do
                    requires :token, type: String, desc: 'Access token.'
                    requires :id, type: String, desc: 'ID of the goal'
                end
                get ':id', root: 'goal' do
                    authenticate!
                    @current_user.goals.where(id: permitted_params[:id]).first!
                end

                desc 'Add a goal'
                params do
                    requires :token, type: String, desc: 'Access token.'
                    requires :goal_name, type: String, desc: 'Name of Goal'
                    requires :start_date, type: DateTime, desc: 'Start date of Goal'
                    requires :description, type: String, desc: 'Description of Goal'
                    requires :repetition, type: Hash do
                        requires :type_of_repetition, type: Integer, desc: 'Type of repetiton. 1: daily, 2:weekly, 3:monthly, 4:yearly'
                        requires :how_often, type: Integer, desc: 'How often repeat'
                        optional :day_of_week, type: String, desc: 'Which days of week'
                        optional :type_of_month, type: String, desc: 'Type of month repetition. 1: same day each month, 2: same day of week and week'
                    end
                    requires :end_date, type: Hash do
                        requires :type_of_end_date, type: Integer, desc: 'Type of end date. 1: forever, 2: until a date, 3: for a number of events'
                        optional :specific_end_date, type: DateTime, desc: 'Specific end date'
                        optional :number_of_event, type: Integer, desc: 'Number of event'
                    end
                    requires :is_auto, type: Boolean, desc: 'Is auto update failed day'
                    requires :time_before, type: String, desc: '0,1,2,3,4,5'
                end
                post do
                    authenticate!
                    @goal = @current_user.goals.create!(goal_name: params[:goal_name], start_date: params[:start_date], description: params[:description], is_auto: params[:is_auto], time_before: params[:time_before])
                    @goal.create_repetition!(type_of_repetition: params[:repetition][:type_of_repetition], how_often: params[:repetition][:how_often])
                    case params[:repetition][:type_of_repetition]
                    when 2 then @goal.repetition.update!(day_of_week: params[:repetition][:day_of_week])
                    when 3 then @goal.repetition.update!(type_of_month: params[:repetition][:type_of_month])
                    end
                    @goal.create_end_date!(type_of_end_date: params[:end_date][:type_of_end_date])
                    case params[:end_date][:type_of_end_date]
                    when 2 then @goal.end_date.update!(specific_end_date: params[:end_date][:specific_end_date])
                    when 3 then @goal.end_date.update!(number_of_event: params[:end_date][:number_of_event])
                    end
                    # Pushlish message to nodejs
                    # $redis.publish 'goalChange', { email: @current_user.email }.to_json
                    @goal.create_track
                    @current_user.goals.all.order(id: :desc)
                end

                desc 'Edit a goal'
                params do
                    requires :token, type: String, desc: 'Access token.'
                    requires :id, type: Integer, desc: 'Id of the Goal'
                    requires :goal_name, type: String, desc: 'Name of Goal'
                    requires :start_date, type: DateTime, desc: 'Start date of Goal'
                    requires :description, type: String, desc: 'Description of Goal'
                    requires :repetition, type: Hash do
                        requires :type_of_repetition, type: Integer, desc: 'Type of repetiton. 1: daily, 2:weekly, 3:monthly, 4:yearly'
                        requires :how_often, type: Integer, desc: 'How often repeat'
                        optional :day_of_week, type: String, desc: 'Which days of week'
                        optional :type_of_month, type: String, desc: 'Type of month repetition. 1: same day each month, 2: same day of week and week'
                    end
                    requires :end_date, type: Hash do
                        requires :type_of_end_date, type: Integer, desc: 'Type of end date. 1: forever, 2: until a date, 3: for a number of events'
                        optional :specific_end_date, type: DateTime, desc: 'Specific end date'
                        optional :number_of_event, type: Integer, desc: 'Number of event'
                    end
                    requires :is_auto, type: Boolean, desc: 'Is auto update failed day'
                    requires :time_before, type: String, desc: '0,1,2,3,4,5'
                end
                put ':id' do
                    authenticate!
                    @goal = @current_user.goals.find(params[:id])
                    @goal.update(goal_name: params[:goal_name], start_date: params[:start_date], description: params[:description], is_auto: params[:is_auto], time_before: params[:time_before])
                    @goal.repetition.update(type_of_repetition: params[:repetition][:type_of_repetition], how_often: params[:repetition][:how_often])
                    case params[:repetition][:type_of_repetition]
                    when 2 then @goal.repetition.update!(day_of_week: params[:repetition][:day_of_week])
                    when 3 then @goal.repetition.update!(type_of_month: params[:repetition][:type_of_month])
                    end
                    @goal.end_date.update(type_of_end_date: params[:end_date][:type_of_end_date])
                    case params[:end_date][:type_of_end_date]
                    when 1 then @goal.end_date.update!(specific_end_date: nil, number_of_event: nil)
                    when 2 then @goal.end_date.update!(specific_end_date: params[:end_date][:specific_end_date], number_of_event: nil)
                    when 3 then @goal.end_date.update!(number_of_event: params[:end_date][:number_of_event], specific_end_date: nil)
                    end
                    # Pushlish message to nodejs
                    # $redis.publish 'goalChange', { email: @current_user.email }.to_json
                    @current_user.goals.find(params[:id]).create_track
                    @current_user.goals.find(params[:id]).del_dates_after_end_date
                    @current_user.goals.all.order(id: :desc)
                end

                desc 'Delete a goal'
                params do
                    requires :token, type: String, desc: 'Access token.'
                    requires :id, type: String, desc: 'IDs of the Goal'
                end
                delete ':id' do
                    authenticate!
                    params[:id].split(/,\s*/).each do |g|
                        @current_user.goals.find(g).destroy
                    end
                    # Pushlish message to nodejs
                    # $redis.publish 'goalChange', { email: @current_user.email }.to_json
                    'OK'
                end

                desc 'Mark a day passed or failed the goal'
                params do
                    requires :token, type: String, desc: 'Access token.'
                    requires :id, type: String, desc: 'ID of the Goal'
                    requires :date_of_calendar, type: DateTime, desc: 'Date'
                    requires :status, type: String, desc: 'Status of the day'
                end

                put ':id/calendar' do
                    authenticate!
                    @goal = @current_user.goals.find(params[:id])
                    @calendars = @goal.calendars
                    if @calendars.exists?(date_of_calendar: params[:date_of_calendar].to_date)
                        @calendars.where(date_of_calendar: params[:date_of_calendar].to_date).update(status: params[:status])
                    else
                        @calendars.create!(date_of_calendar: params[:date_of_calendar].to_date, status: params[:status])
                    end
                    @current_user.goals.find(params[:id]).create_track
                    @current_user.goals.find(params[:id])
                end

                desc 'Set a goal is favourite'
                params do
                    requires :token, type: String, desc: 'Access token.'
                    requires :id, type: String, desc: 'ID of the Goal'
                end
                put ':id/favorite' do
                    authenticate!
                    @goal = @current_user.goals.find(params[:id])
                    @goal.update!(is_favorite: !@goal.is_favorite)
                    @goal.is_favorite
                end

                desc 'Archive a goal'
                params do
                    requires :token, type: String, desc: 'Access token.'
                    requires :id, type: String, desc: 'IDs of the Goal'
                end
                put ':id/archive' do
                    authenticate!
                    params[:id].split(/,\s*/).each do |g|
                        @goal = @current_user.goals.find(g)
                        @goal.update!(is_archived: !@goal.is_archived)
                    end
                    'OK'
                end
            end
    end
  end
end
