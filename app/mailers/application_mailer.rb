class ApplicationMailer < ActionMailer::Base
  default from: 'musicappintern@gmail.com'
  layout 'mailer'
end
