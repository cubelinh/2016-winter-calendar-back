class UserMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_mailer.sent_forgot_password.subject
  #
  def sent_forgot_password(user)
    @user = user
    mail to: user.email, subject: "Reset password at http://seinfeld-calendar-v1.herokuapp.com/"
  end

  def sent_active_link(user)
    @user = user
    mail to: user.email, subject: "Active account at http://seinfeld-calendar-v1.herokuapp.com/"
  end
end
