class Goal < ApplicationRecord
    belongs_to :myuser
    has_one :end_date, dependent: :destroy
    has_one :repetition, dependent: :destroy
    has_many :reminders, dependent: :delete_all
    has_many :calendars, dependent: :delete_all
    attr_accessor :daily_flag, :weekly_flag, :monthly_flag, :annual_flag

    def is_the_date_of_goal?(date)
        return false unless date.is_a?(Date)
        return false if date < start_date.to_date
        daily_flag = true
        weekly_flag = true
        monthly_flag = true
        annual_flag = true

        case repetition.type_of_repetition
        when 1 then # daily
            daily_flag = is_the_date_if_daily?(date)
        when 2 then # weekly
            weekly_flag = is_the_date_if_weekly?(date)
        when 3 then # monthly
            monthly_flag = is_the_date_if_monthly?(date)
        when 4 then # annual
            annual_flag = is_the_date_if_annual?(date)
        end

        return true if daily_flag && weekly_flag && monthly_flag && annual_flag

        false
    end

    private

    def is_the_date_if_daily?(date)
        return true if (date - start_date.to_date).to_i % repetition.how_often == 0
        false
    end

    def is_the_date_if_weekly?(date)
        # If the week repeat properly
        day_dif = (to_monday(date) - to_monday(start_date.to_date)).to_i
        flag_repeat = (day_dif / 7) % repetition.how_often == 0

        # If day of week is included in db
        # day_week = Date::DAYNAMES[date.wday][0..2]
        day_week = date.wday
        flag_dayweek = repetition.day_of_week.to_s.include?(day_week.to_s)

        # return the combination of two flag
        flag_repeat && flag_dayweek
    end

    def is_the_date_if_monthly?(date)
        # Is the month repeat properly
        year_dif = date.strftime('%Y').to_i - start_date.to_date.strftime('%Y').to_i
        date_month = date.strftime('%m').to_i
        start_date_month = start_date.to_date.strftime('%m').to_i
        flag_repeat = (year_dif * 12 + date_month - start_date_month) % repetition.how_often == 0

        flag_day_month = false
        case repetition.type_of_month
        when 1 then # On the same day of the start date
            if date.strftime('%d').to_i == start_date.to_date.strftime('%d').to_i
                flag_day_month = true
            end
        when 2 then # On the same nrd day of week of the start date
            d_date = date.strftime('%d').to_i
            d_start_date = start_date.to_date.strftime('%d').to_i
            if (date.wday == start_date.to_date.wday) && ((d_date - 1) / 7 == (d_start_date - 1) / 7)
                flag_day_month = true
            end
        end

        # return the combination of two flag
        flag_repeat && flag_day_month
    end

    def is_the_date_if_annual?(date)
        year_date = date.strftime('%Y').to_i
        year_start_date = start_date.to_date.strftime('%Y').to_i
        flag_repeat = (year_date - year_start_date) % repetition.how_often == 0
        flag_same_day = date.strftime('%m-%d').eql? start_date.to_date.strftime('%m-%d')
        flag_repeat && flag_same_day
    end

    def to_monday(date)
        if date.wday == 0
            date - 6
        else
            date - (date.wday - 1)
        end
    end

    public

    def test(date)
        return true if (date - start_date.to_date).to_i % repetition.how_often == 0
        false
    end

    def del_dates_after_end_date
      calendars.where('calendars.date_of_calendar > ?', end_date.specific_end_date).destroy_all
    end

    def create_track
        unless is_archived
            track = case repetition.type_of_repetition
                    when 1
                        progress_daily
                    when 2
                        progress_weekly
                    when 3
                        progress_monthly
                    when 4
                        progress_yearly
            end
            Goal.find(id).update!(progress: track[:progress_temp])
            Goal.find(id).update!(best_chain: track[:best_chain_temp])
            Goal.find(id).update!(current_chain: track[:current_temp])
        end
    end

    def progress_daily
        count_passed = 0
        count = 0
        start_date_temp = start_date.to_date
        current_temp = 0
        best_chain_temp = 0
        while Time.current.to_date >= start_date_temp && (end_date.specific_end_date.nil? || start_date_temp <= end_date.specific_end_date)
            count += 1
            end_date.update!(specific_end_date: start_date_temp) if (count == end_date.number_of_event) && (end_date.type_of_end_date == 3)

            if calendars.exists?(date_of_calendar: start_date_temp)
                if calendars.where(date_of_calendar: start_date_temp).first.status == 1
                    count_passed += 1
                    current_temp += 1
                elsif Time.current.to_date != start_date_temp
                    current_temp = 0
                end
            elsif Time.current.to_date != start_date_temp
                calendars.create!(date_of_calendar: start_date_temp, status: 2) if is_auto == true
                current_temp = 0
            end
            best_chain_temp = current_temp if current_temp > best_chain_temp
            start_date_temp += repetition.how_often.days
        end
        { current_temp: current_temp, best_chain_temp: best_chain_temp, progress_temp: count_passed.to_s + '/' + count.to_s }
    end

    def progress_weekly
        week = repetition.day_of_week.split(/,\s*/)
        count_passed = 0
        count = 0
        start_date_temp = start_date.to_date
        every = repetition.how_often
        every -= 1
        current_temp = 0
        best_chain_temp = 0
        begin
            if week.include? start_date_temp.wday.to_s
                count += 1
                end_date.update!(specific_end_date: start_date_temp) if (count == end_date.number_of_event) && (end_date.type_of_end_date == 3)
                if calendars.exists?(date_of_calendar: start_date_temp)
                    if calendars.where(date_of_calendar: start_date_temp).first.status == 1
                        count_passed += 1
                        current_temp += 1
                    elsif Time.current.to_date != start_date_temp
                        current_temp = 0
                    end
                elsif Time.current.to_date != start_date_temp
                    calendars.create!(date_of_calendar: start_date_temp, status: 2) if is_auto == true
                    current_temp = 0
                end
                best_chain_temp = current_temp if current_temp > best_chain_temp
            end
            start_date_temp += 1.day
        end while start_date_temp.wday != 0 && Time.current.to_date >= start_date_temp && (end_date.specific_end_date.nil? || start_date_temp <= end_date.specific_end_date)
        while Time.current.to_date >= start_date_temp && (end_date.specific_end_date.nil? || start_date_temp <= end_date.specific_end_date)
            start_date_temp += every.weeks
            begin
                if (Time.current.to_date >= start_date_temp && (end_date.specific_end_date.nil? || start_date_temp <= end_date.specific_end_date)) && (week.include? start_date_temp.wday.to_s)
                    count += 1
                    end_date.update!(specific_end_date: start_date_temp) if (count == end_date.number_of_event) && (end_date.type_of_end_date == 3)
                    if calendars.exists?(date_of_calendar: start_date_temp)
                        if calendars.where(date_of_calendar: start_date_temp).first.status == 1
                            count_passed += 1
                            current_temp += 1
                        elsif Time.current.to_date != start_date_temp
                            current_temp = 0
                        end
                    elsif Time.current.to_date != start_date_temp
                        calendars.create!(date_of_calendar: start_date_temp, status: 2) if is_auto == true
                        current_temp = 0
                    end
                    best_chain_temp = current_temp if current_temp > best_chain_temp
                end
                start_date_temp += 1.day
            end while start_date_temp.wday != 0 && Time.current.to_date >= start_date_temp && (end_date.specific_end_date.nil? || start_date_temp <= end_date.specific_end_date)

        end
        { current_temp: current_temp, best_chain_temp: best_chain_temp, progress_temp: count_passed.to_s + '/' + count.to_s }
    end

    def progress_monthly
        count_passed = 0
        count = 0
        start_date_temp = start_date.to_date
        current_temp = 0
        best_chain_temp = 0
        if repetition.type_of_month == 1
            start_date_temp_const = start_date.to_date
            while Time.current.to_date >= start_date_temp && (end_date.specific_end_date.nil? || start_date_temp <= end_date.specific_end_date)
                count += 1
                end_date.update!(specific_end_date: start_date_temp) if (count == end_date.number_of_event) && (end_date.type_of_end_date == 3)
                if calendars.exists?(date_of_calendar: start_date_temp)
                    if calendars.where(date_of_calendar: start_date_temp).first.status == 1
                        count_passed += 1
                        current_temp += 1
                    elsif Time.current.to_date != start_date_temp
                        current_temp = 0
                    end
                elsif Time.current.to_date != start_date_temp
                    calendars.create!(date_of_calendar: start_date_temp, status: 2) if is_auto == true
                    current_temp = 0
                end
                best_chain_temp = current_temp if current_temp > best_chain_temp
                temp1 = repetition.how_often
                temp1 *= count
                start_date_temp = start_date_temp_const + temp1.months
            end
        else
            week = start_date_temp.week_of_month - 1
            day = start_date_temp.wday
            while Time.current.to_date >= start_date_temp && (end_date.specific_end_date.nil? || start_date_temp <= end_date.specific_end_date)
                count += 1
                end_date.update!(specific_end_date: start_date_temp) if (count == end_date.number_of_event) && (end_date.type_of_end_date == 3)
                week_splited = start_date_temp.week_split
                if !week_splited[week].nil? && !week_splited[week][day].nil? && (Time.current.to_date >= start_date_temp.change(day: week_splited[week][day]))
                    start_date_temp = start_date_temp.change(day: week_splited[week][day])
                    if calendars.exists?(date_of_calendar: start_date_temp)
                        if calendars.where(date_of_calendar: start_date_temp).first.status == 1
                            count_passed += 1
                            current_temp += 1
                        elsif Time.current.to_date != start_date_temp
                            current_temp = 0
                        end
                    elsif Time.current.to_date != start_date_temp
                        calendars.create!(date_of_calendar: start_date_temp, status: 2) if is_auto == true
                        current_temp = 0
                    end
                    best_chain_temp = current_temp if current_temp > best_chain_temp
                end
                start_date_temp += repetition.how_often.months
            end
        end
        { current_temp: current_temp, best_chain_temp: best_chain_temp, progress_temp: count_passed.to_s + '/' + count.to_s }
    end

    def progress_yearly
        count_passed = 0
        count = 0
        current_temp = 0
        best_chain_temp = 0
        start_date_temp = start_date.to_date
        while Time.current.to_date >= start_date_temp && (end_date.specific_end_date.nil? || start_date_temp <= end_date.specific_end_date)
            count += 1
            end_date.update!(specific_end_date: start_date_temp) if (count == end_date.number_of_event) && (end_date.type_of_end_date == 3)
            if calendars.exists?(date_of_calendar: start_date_temp)
                if calendars.where(date_of_calendar: start_date_temp).first.status == 1
                    count_passed += 1
                    current_temp += 1
                elsif Time.current.to_date != start_date_temp
                    current_temp = 0
                end
            elsif Time.current.to_date != start_date_temp
                calendars.create!(date_of_calendar: start_date_temp, status: 2) if is_auto == true
                current_temp = 0
            end
            best_chain_temp = current_temp if current_temp > best_chain_temp
            start_date_temp += repetition.how_often.years
        end
        { current_temp: current_temp, best_chain_temp: best_chain_temp, progress_temp: count_passed.to_s + '/' + count.to_s }
    end
end
