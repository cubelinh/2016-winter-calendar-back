class ApiKey < ActiveRecord::Base
    # attr_accessor :access_token, :expires_at, :myuser_id, :active, :application
    before_create :generate_access_token
    before_create :set_expiration
    belongs_to :myuser

    def expired?
        DateTime.now >= expires_at
    end

    private

    def generate_access_token
        self.access_token = SecureRandom.hex
    end

    #
    def set_expiration
        self.expires_at = DateTime.now + 30
    end
end
