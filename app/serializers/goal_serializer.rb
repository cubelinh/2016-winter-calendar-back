class GoalSerializer < ActiveModel::Serializer
    attributes :id, :goal_name, :start_date, :description, :is_favorite, :is_archived, :is_auto, :current_chain, :best_chain, :progress, :end_date, :repetition, :calendars, :time_before
end
