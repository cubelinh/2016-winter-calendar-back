require 'test_helper'

class UserMailerTest < ActionMailer::TestCase
  test "sent_forgot_password" do
    mail = UserMailer.sent_forgot_password
    assert_equal "Sent forgot password", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end
