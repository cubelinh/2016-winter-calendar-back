# Preview all emails at http://localhost:3000/rails/mailers/user_mailer
class UserMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/user_mailer/sent_forgot_password
  def sent_forgot_password
    UserMailer.sent_forgot_password
  end

end
